$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:features/ebayHome.feature");
formatter.feature({
  "name": "Les fonnctionalites de ebayHomePage",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "scenario qui teste la recherche avance",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@scenario1"
    }
  ]
});
formatter.step({
  "name": "Je suis sur la page dacceuil",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.EbayHome_Steps.je_suis_sur_la_page_dacceuil()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Je clique sur le lien recherche avancee",
  "keyword": "When "
});
formatter.match({
  "location": "steps.EbayHome_Steps.je_clique_sur_le_lien_recherche_avancee()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Je navigue vers la page recherche avancee",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.EbayHome_Steps.je_navigue_vers_la_page_recherche_avancee()"
});
formatter.result({
  "status": "passed"
});
});