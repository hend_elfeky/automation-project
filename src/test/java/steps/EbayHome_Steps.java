package steps;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class EbayHome_Steps {
	WebDriver driver ;
	@Given("Je suis sur la page dacceuil")
	public void je_suis_sur_la_page_dacceuil() {
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.ebay.ca/");
		
	    
	}

	@When("Je clique sur le lien recherche avancee")
	public void je_clique_sur_le_lien_recherche_avancee() {
		driver.findElement(By.id("gh-as-a")).click();
	    
	}

	@Then("Je navigue vers la page recherche avancee")
	public void je_navigue_vers_la_page_recherche_avancee() throws InterruptedException {
		String urlattendu="https://www.ebay.ca/sch/ebayadvsearch";
		String Titreattendu="eBay Search: Advanced Search";
		String urlObtenu=driver.getCurrentUrl();
		String titreObtenu=driver.getTitle();
		if(!titreObtenu.equalsIgnoreCase(titreObtenu)) {
			fail("Le titre de la pag n'est pas celui attendu");
		}
		Assert.assertEquals(urlattendu, urlObtenu);
		Thread.sleep(4000);
		driver.quit();
	    
	}




}
