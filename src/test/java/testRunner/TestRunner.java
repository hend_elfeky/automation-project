package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features= {"features"}, //chemin du dossier du features
		glue= {"steps"} ,//chemin du dossier du glue code(steps definitions)
		dryRun=false, //true:chercher les features qui n'ont pas de glue code et les runner
		monochrome=true, //formatter l'affichage dans la console
		tags="@scenario1",
	//tags="@smoke1 or @tag1" ,//selectionner els scenario a excuter
	//name="valid",
	//plugin= {"pretty","html:report1","json:report2"}
	    plugin= {"pretty","json:target/json-report/cucumber.json"}
		
		


		
		)

public class TestRunner {
	

}
